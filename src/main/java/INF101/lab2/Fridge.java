package INF101.lab2;

import java.util.NoSuchElementException;


import java.util.ArrayList;



public class Fridge implements IFridge{

    ArrayList<FridgeItem> fridgeItems = new ArrayList<FridgeItem>();
    
    
    /*public void int nItemsInFridge(ArrayList<FridgeItem> fridgeitems){
        int count = 0;
        for (FridgeItem item : fridgeitems) {
            count++;
            return count;

        }
    return count;*/

    public int nItemsInFridge(){
        //int counter = fridgeItems.size();
        //System.out.println(fridgeItems);
        return fridgeItems.size();

    }
    
    public int totalSize() {
        //int space = 20-fridgeItems.size();
        //System.out.println(space);
        return 20;
    }

    public boolean placeIn(FridgeItem item){
        if (fridgeItems.size()<20){
            fridgeItems.add(item);
            return true;
        }
        else {
            return false; //fridgeItems.contains(bro);
            }
    }

    public void takeOut(FridgeItem item) {//throws NoSuchElementException
        if (fridgeItems.size()>0 && fridgeItems.contains(item)){
            fridgeItems.remove(item);
            //throw new NoSuchElementException();
        }
        else{
        //fridgeItems.remove(item);
        throw new NoSuchElementException();
        }
    }

    public void emptyFridge(){
        fridgeItems.clear();

    }
    

    public ArrayList<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expired = new ArrayList<FridgeItem>();
    
        for (FridgeItem food : fridgeItems) {
            if (food.hasExpired()){
                expired.add(food);
            }
        }
        for (FridgeItem food : expired) {
            takeOut(food);
        }     
    return expired;
    }
}

//class main {
   // public static void main(String[] args) {
        //Fridge
        
   // }
//}
